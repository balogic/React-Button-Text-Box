import React from 'react';
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      txt: 'Default Text',
      count: 0
    }
    this.update = this.update.bind(this);
  }
  update( e ) {
    this.setState({
      txt: (e.target.value),
      count: (this.state.count + 1)
    })
  }
  render() {
    return (
      <div>
        <Widget update={this.update}/>
      <h1>{this.state.txt}</h1>
    <Button update={this.update} count={this.state.count}/>
      </div>
    )
  }
}

const Button = (props) => <button onClick={props.update}>{props.count}</button>
const Widget = (props) => <input type="text" onChange={props.update}/>

export default App;
